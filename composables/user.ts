import {useStorage} from "@vueuse/core"

// build composable which represents data from localstorage "user": {id, login, emain, name, surname}
export const useUser = () => {
    const user = useStorage("user", {
        id: 1,
        login: "",
        email: "",
        name: "Руслан",
        surname: ""
    })
    return {user}
}