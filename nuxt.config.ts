// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    '@vueuse/nuxt'
  ],
  routeRules: {
    // Homepage pre-rendered at build time
    '/': { prerender: true },
    // Product page generated on-demand, revalidates in background
    '/drugs/**': { swr: 3600 },
    // Product page generated on-demand, revalidates in background
    '/places/**': { swr: true },
  }
})
